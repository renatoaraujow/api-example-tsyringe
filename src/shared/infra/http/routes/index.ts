import { Router } from "express";

import { usersRoutes } from "./users.routes";

const router = Router();

router.get("/", (request, response) => {
  response.send(
    `<h1 style="font-family: Arial;">Bem-vindo a API Example Tsyringe!</h1>
    <p style="font-family: Arial; font-size: 16px;">Para acessar a documentação da API <a href="/api-docs">clique aqui</a>.</p>`
  );
});

router.use("/users", usersRoutes);

export { router };
